use sakila;
select f.name,month(b.payment_date) as bulan,
count(amount),sum(amount),avg(amount), min(amount), max(amount)
from rental a 
inner join payment b on b.rental_id=a.rental_id
inner join inventory c on c.inventory_id=a.inventory_id
inner join film d on d.film_id=c.film_id
inner join film_category e on e.film_id=d.film_id
inner join category f on f.category_id=e.category_id
where amount > 0
group by bulan,f.name
having count(amount) > 150
order by f.name,bulan -- ,d.title
