DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spFilterByActor`(IN lastName varchar(100))
BEGIN
select `a`.`last_name` AS `last_name`,`c`.`title` AS `title` from 
`sakila`.`actor` `a` join `sakila`.`film_actor` `b` on `b`.`actor_id` = `a`.`actor_id`
 join `sakila`.`film` `c` on `c`.`film_id` = `b`.`film_id`
 where a.last_name like CONCAT('%', lastName , '%');
END$$
DELIMITER ;
